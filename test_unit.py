import unittest
from calculadora_lib import suma

class TestCalculadora(unittest.TestCase):

	def test_suma(self):
		# arrange
		a = 3
		b = 2
		c = 5
		# act
		resultado = suma(a, b)
		# assert
		self.assertEqual(resultado, c)			


if __name__ == "__main__":
	unittest.main()